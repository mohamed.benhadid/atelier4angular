import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-fils-component',
  templateUrl: './fils-component.component.html',
  styleUrls: ['./fils-component.component.css']
})
export class FilsComponentComponent implements OnInit {
  @Input() colorfils;

  constructor() { }

  ngOnInit(): void {
  }

}
